//test1.cpp

#include <iostream>
#include <cassert>
#include "Board.h"


int main() {

  Board b;

  assert(b.add_piece("d7",'K'));
  assert(b.add_piece("a1",'K'));
  assert( ! b.add_piece("d7",'R'));
  assert( ! b.add_piece("i1",'k'));
  assert( ! b.add_piece("a0",'k'));
  assert( ! b.add_piece("a9",'k'));
  assert( ! b.add_piece("e3",'q'));
  
  assert( ! b.remove_piece("b1"));
  assert( ! b.remove_piece("i2"));
  assert( ! b.remove_piece("b9"));
  assert(b.remove_piece("d7"));
  assert(b.add_piece("d7",'R'));

  std::cout << "All assertions passed!" << std::endl;
}
