/*
  King.h
  Alice Yang
  ayang36
  HW7
*/
#ifndef KING_H
#define KING_H

#include <string>
#include "Piece.h"

class King: public Piece {

public:

  //Creates a new piece
  King(): Piece() {}

  //Whether the move is legal or not
  bool legal_move_shape(const std::string& start,
                        const std::string& end) const;
};

#endif
