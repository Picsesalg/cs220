/*
  King.cpp
  Alice Yang
  ayang36
  HW7
*/
#include <string>
#include <cmath>
#include <iostream>
#include "Piece.h"
#include "King.h"

using std::string;

bool King::legal_move_shape(const string& start,
			    const string& end) const{
  //Start col
  char start_col = start.at(0);
  //Start row
  char start_row = start.at(1);
  //End col
  char end_col = end.at(0);
  //End row
  char end_row = end.at(1);
  
  if ((abs(start_col - end_col) == 1 || abs(start_col - end_col) == 0) && (abs(start_row - end_row) == 1 || abs(start_row - end_row) == 0)) {
    return true;
  }
  else {
    return false;
  }
}
