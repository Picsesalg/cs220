/*
  Rook.cpp
  Alice Yang
  ayang36
  HW7
*/
#include <string>
#include "Piece.h"
#include "Rook.h"

using std::string;

bool Rook::legal_move_shape(const string& start,
			    const string& end) const {
  //Start col
  char start_col = start.at(0);
  //Start row
  char start_row = start.at(1);
  //End col
  char end_col = end.at(0);
  //End row
  char end_row = end.at(1);

  if (start_col == end_col || start_row == end_row) {
    return true;
  }
  else {
    return false;
  }
}
