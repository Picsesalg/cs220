/*
  Rook.h
  Alice Yang
  ayang36
  HW7
*/
#ifndef ROOK_H
#define ROOK_H

#include <string>
#include "Piece.h"

class Rook: public Piece {

public:

  //Create a new rook
 Rook(): Piece() {}

  //returns true if it's legal
  //returns false if it's not
  bool legal_move_shape(const std::string& start,
			const std::string& end) const;
};

#endif
