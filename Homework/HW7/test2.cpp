//test2.cpp

#include <iostream>
#include <cassert>
#include "Piece.h"
#include "King.h"
#include "Rook.h"

int main() {

  Piece p;
  assert( ! p.get_has_moved());
  assert( ! p.legal_move_shape("a1","a2"));
  p.set_has_moved(); 
  assert(p.get_has_moved());
  
  King k;
  assert( ! k.get_has_moved());
  k.set_has_moved(); 
  assert(k.get_has_moved()); 
  assert(k.legal_move_shape("a1","a2"));
  assert(k.legal_move_shape("a1","b1"));
  assert(k.legal_move_shape("a1","b2")); 
  assert(! k.legal_move_shape("a1","a3"));
  assert(! k.legal_move_shape("a1","c3"));

  Rook r;
  assert( ! r.get_has_moved());
  r.set_has_moved(); 
  assert(r.get_has_moved()); 
  assert(r.legal_move_shape("a1","a2"));
  assert(r.legal_move_shape("a1","b1"));
  assert(r.legal_move_shape("a1","h1"));
  assert(r.legal_move_shape("a1","a3"));
  assert(r.legal_move_shape("a1","a7"));
  assert(! r.legal_move_shape("a1","b2"));
  
  std::cout << "All assertions passed!"	<< std::endl;
}
