#ifndef HELPER_H
#define HELPER_H

std::pair<std::string, size_t> find_dir_and_length(std::string start_loc, std::string end_loc);

#endif
