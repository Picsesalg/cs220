/*
  hw7.cpp
  Alice Yang
  ayang36
  HW7
*/

#include <string>
#include <iostream>
#include <map>
#include <cmath>
#include "Piece.h"
#include "King.h"
#include "Rook.h"
#include "Board.h"
#include "helper.h"

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::pair;
using std::make_pair;
using std::map;

int main() {
  //Initialising the board
  Board chessboard;
  chessboard.add_piece("b3", 'K');
  chessboard.add_piece("b4", 'R');
  chessboard.add_piece("b6", 'k');
  chessboard.add_piece("e3", 'r');
  //Initialising the pieces
  King wk, bk;
  Rook wr, br;
  //Getting user input
  string start_loc;
  cin >> start_loc;
  string end_loc;
  cin >> end_loc;
  //End location colour
  pair<bool, bool> end_loc_colour;
  end_loc_colour = chessboard.color_at(end_loc);

  //White King
  if (start_loc.compare("b3") == 0) {
    //If the move is illegal for kings    
    if (!wk.legal_move_shape(start_loc, end_loc)) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If piece at end_loc is white
    if (end_loc_colour.first == true && end_loc_colour.second == true) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If the piece goes off the board
    if ((end_loc.at(0) < 'a' || end_loc.at(0) > 'h') || (end_loc.at(1) < '1' || end_loc.at(1) > '8')) {
      cout << "NOT legal" << endl;
      return 1;
    }
    else {
      chessboard.add_piece(end_loc, 'K');
      chessboard.remove_piece(start_loc);
      wk.set_has_moved();
      cout << "legal" << endl;
      return 0;
    }
  }
  //Black King
  else if (start_loc.compare("b6") == 0) {
    //If the move is illegal for kings
    if (!wk.legal_move_shape(start_loc, end_loc)) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If piece at end_loc is black
    if (end_loc_colour.first == true && end_loc_colour.second == false) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If the piece goes off the board
    if ((end_loc.at(0) < 'a' || end_loc.at(0) > 'h') || (end_loc.at(1) < '1' || end_loc.at(1) > '8')) {
      cout << "NOT legal" << endl;
      return 1;
    }
    else {
      chessboard.add_piece(end_loc, 'k');
      chessboard.remove_piece(start_loc);
      bk.set_has_moved();
      cout << "legal" << endl;
      return 0;
    }
  }
  //White Rook
  else if (start_loc.compare("b4") == 0) {
    //If the move is legal for rooks
    if (!wr.legal_move_shape(start_loc, end_loc)) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If piece at end_loc is white
    if (end_loc_colour.first == true && end_loc_colour.second == true) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //Determining direction and length
    string dir = find_dir_and_length(start_loc, end_loc).first;
    size_t length = find_dir_and_length(start_loc, end_loc).second;
    
    pair<bool, bool> path = chessboard.path_is_legal_and_clear(start_loc, dir, length);
    //If direction invalid or goes off the board
    if (path.first == false && path.second == false) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If something is on the path    
    else if (path.first == true && path.second == false) {
      cout << "NOT legal" << endl;
      return 1;
    }
    else {
      chessboard.add_piece(end_loc, 'R');
      chessboard.remove_piece(start_loc);
      wr.set_has_moved();
      cout << "legal" << endl;
      return 0;
    }
  }
  //Black Rook
  else if (start_loc.compare("e3") == 0) {
    //If the move is legal for rooks
    if (!wr.legal_move_shape(start_loc, end_loc)) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If piece at end_loc is black
    if (end_loc_colour.first == true && end_loc_colour.second == false) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //Determining direction and length
    string dir;
    int temp_length;
    size_t length;
    if (start_loc.at(0) == end_loc.at(0)) {
      temp_length = end_loc.at(1) - start_loc.at(1);
      if (temp_length > 0) {
	dir = "n";
      }
      else if (temp_length < 0) {
	dir = "s";
      }
      length = abs(temp_length);
    }
    else if (start_loc.at(1) == end_loc.at(1)) {
      temp_length = end_loc.at(0) - start_loc.at(0);
      if (temp_length > 0) {
	dir = "e";
      }
      else if (temp_length < 0) {
	dir = "w";
      }
      length = abs(temp_length);
    }
    pair<bool, bool> path = chessboard.path_is_legal_and_clear(start_loc, dir, length);    
    //If direction invalid or goes off the board
    if (path.first == false && path.second == false) {
      cout << "NOT legal" << endl;
      return 1;
    }
    //If something is on the path    
    else if (path.first == true && path.second == false) {
      cout << "NOT legal" << endl;
      return 1;
    }
    else {
      chessboard.add_piece(end_loc, 'r');
      chessboard.remove_piece(start_loc);
      br.set_has_moved();
      cout << "legal" << endl;
      return 0;
    }
  }
  //Anything else
  else {
    cout << "NOT legal" << endl;
    return 1;
  }
  return 0;
}
