//test3.cpp

#include <iostream>
#include <cassert>
#include "Board.h"
#include "Piece.h"
#include "King.h"
#include "Rook.h"

using std::cout;
using std::endl;
using std::make_pair;

int main() {

  Board b;

  King wk;
  assert(b.add_piece("a1",'K'));
  King bk;
  assert(b.add_piece("b2",'k'));
  Rook wr;
  assert(b.add_piece("b8",'R'));
  
  cout << b.to_string() << endl;

  //make a move from a1 to b1
  assert(wk.legal_move_shape("a1","b1"));
  assert(b.color_at("b1") == make_pair(false,false)); //no piece at destination
  assert(b.remove_piece("a1"));
  assert(b.add_piece("a2",'K'));
  wk.set_has_moved();
  assert(wk.get_has_moved());
  assert(! bk.get_has_moved());

  cout << b.to_string() << endl;

  //make a move from b8 to b2
  assert(wr.legal_move_shape("b8","b2"));
  assert(b.path_is_legal_and_clear("b8","s",5) == make_pair(true,true));
  assert(b.color_at("b2") == make_pair(true,false));  //opponent piece exists at destination
  assert(b.remove_piece("b2")); //remove captured piece
  assert(b.remove_piece("b8"));
  assert(b.add_piece("b2",'R'));
  wr.set_has_moved();
  assert(wr.get_has_moved());

  cout << b.to_string() << endl;
  
  std::cout << "All assertions passed!" << std::endl;
}
