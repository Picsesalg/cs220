#include <string>
#include "helper.h"

using std::pair;
using std::make_pair;
using std::string;

pair<string, size_t> find_dir_and_length(string start_loc, string end_loc) {
  string dir;
  int temp_length;
  size_t length;
  if (start_loc.at(0) == end_loc.at(0)) {
    temp_length = end_loc.at(1) - start_loc.at(1);
    if (temp_length > 0) {
      dir = "n";
    }
    else if (temp_length < 0) {
      dir = "s";
    }
    length = abs(temp_length);
  }
  else if (start_loc.at(1) == end_loc.at(1)) {
    temp_length = end_loc.at(0) - start_loc.at(0);
    if (temp_length > 0) {
      dir = "e";
    }
    else if (temp_length < 0) {
      dir = "w";
    }
    length = abs(temp_length);
  }
  pair<string, size_t> return_value = make_pair(dir, length);
  return return_value;
}
