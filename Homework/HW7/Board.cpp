/*
  Board.cpp
  Alice Yang
  ayang36
  HW7
*/

#include <string>
#include <utility>
#include <map>
#include <iostream>
#include "Board.h"

using std::string;
using std::map;
using std::pair;
using std::make_pair;

//Reports whether a piece exists at the given location
pair<bool, bool> Board::color_at(string loc) const {
  //The return value
  pair<bool, bool> return_value;
  //Iterates through the board map
  map<string, char>::const_iterator it;

  it = occ.find(loc);
  
  //If the position has white
  if (it->second == 'K' || it->second == 'R') {
    return_value = make_pair(true, true);
  }
  //If the position has black
  else if (it->second == 'k' || it->second == 'r') {
    return_value = make_pair(true, false);
  }
  //If the position has nothing
  else {
    return_value = make_pair(false, false);
  }
  
  return return_value;
}

pair<bool, bool> Board::path_is_legal_and_clear(string start_loc,
						string dir,
						size_t length) const {
  //The return value
  pair<bool, bool> return_value;
  //Starting column
  char start_col = start_loc.at(0);
  //Starting row
  char start_row = start_loc.at(1);
  //Final row
  char end_row;
  //Final column
  char end_col;
  //Final location
  string end_loc;

  //Check if Start Location is valid
  if (start_col < 'a' || start_col > 'h') {
    return_value = make_pair(false, false);
    return return_value;
  }
  else if (start_row < '1' || start_row > '8') {
    return_value = make_pair(false, false);
    return return_value;
  }

  switch(dir.at(0)) {
  case 'n':
    end_col = start_col;
    end_row = start_row;
    //Finding if there is a piece
    for (int i = 1; i < (int) length; i++) {
      end_row = start_row + i;
      end_loc.push_back(end_col);
      end_loc.push_back(end_row);
      return_value = color_at(end_loc);
      if (return_value.first == true) {
	return_value = make_pair(true, false);
	return return_value;
      }
      end_loc = "";
    }
    break;
  case 's':
    end_col = start_col;
    end_row = start_row;
    //Finding if there is a piece
    for (int i = 1; i < (int) length; i++) {
      end_row = start_row - i;
      end_loc.push_back(end_col);
      end_loc.push_back(end_row);
      return_value = color_at(end_loc);
      if (return_value.first == true) {
	return_value = make_pair(true, false);
	return return_value;
      }
      end_loc = "";
    }
    break;
  case 'e':
    end_col = start_col;
    end_row = start_row;
    //Finding if there is a piece
    for (int i = 1; i < (int) length; i++) {
      end_col = start_col + i;
      end_loc.push_back(end_col);
      end_loc.push_back(end_row);
      return_value = color_at(end_loc);
      if (return_value.first == true) {
	return_value = make_pair(true, false);
	return return_value;
      }
      end_loc = "";
    }
    break;
  case 'w':
    end_col = start_col;
    end_row = start_row;
    //Finding if there is a piece
    for (int i = 1; i < (int) length; i++) {
      end_col = start_col - i;
      end_loc.push_back(end_col);
      end_loc.push_back(end_row);
      return_value = color_at(end_loc);
      if (return_value.first == true) {
	return_value = make_pair(true, false);
	return return_value;
      }
      end_loc = "";
    }
    break;
  default:
    return_value = make_pair(false, false);
    return return_value;
    break;
  }
  return_value = make_pair(true, true);  
  return return_value;
}
  
//Adds a piece to the board at the location
bool Board::add_piece(string loc, char piece_type) {
  //Row
  int row = loc.at(1) - '0';
  //Column
  char col = loc.at(0);

  //Whether or not the location is on the board
  if (col < 'a' || col > 'h') {
    return false;
  }
  else if (row < 1 || row > 8) {
    return false;
  }

  //If the piece is valid or not
  switch(piece_type) {
  case 'K':
    break;
  case 'k':
    break;
  case 'R':
    break;
  case 'r':
    break;
  default:
    return false;
    break;
  }

  //If the position is occupied or not
  map<string, char>::iterator it;
  it = occ.find(loc);
  if (it != occ.end()) {
    return false;
  }
  else {
    occ[loc] = piece_type;
    return true;
  }
}

//Remove a piece from the board
bool Board::remove_piece(string loc) {
  map<string, char>::iterator it;
  it = occ.find(loc);
  if (it != occ.end()) {
    occ.erase(it);
    return true;
  }
  else {
    return false;
  }
}

//Returns a string representation of the current board state
string Board::to_string() const {
  const string col_names("abcdefgh");
  const string row_names("12345678");
  string result;

  //append column headers across the top
  result.push_back(' ');
  result.push_back(' ');
  result.push_back(' ');
  for(string::const_iterator it = col_names.begin();
      it != col_names.end(); ++it) {
    result.push_back(' ');
    result.push_back(*it);
    result.push_back(' ');
  }
  result.push_back('\n');

  //append header line below column header line
  result.push_back(' ');
  result.push_back(' ');
  result.push_back(' ');
  for(string::const_iterator it = col_names.begin();
      it != col_names.end(); ++it) {
    result.push_back('-');
    result.push_back('-');
    result.push_back('-');
  }
  result.push_back('\n');
  
  //loop over each row from 8 down to 1
  for(string::const_reverse_iterator row_it = row_names.crbegin();
	row_it != row_names.crend(); ++row_it) {

    //output row header on left side of this row
    result.push_back(*row_it);
    result.push_back(' ');
    result.push_back('|');
    
    //loop over each location in this row
    for(string::const_iterator col_it = col_names.cbegin();
	col_it != col_names.cend(); ++col_it) {

      //deference iterators to create the location name
      string coord;
      coord.push_back(*col_it);
      coord.push_back(*row_it);

      //determine if the current location appears in map
      map<string, char>::const_iterator location = occ.find(coord);

      //output the piece type, if one exists, or a blank
      result.push_back(' ');
      if (location != occ.end()) {
	result.push_back(location->second);
      } else {
	result.push_back(' ');
      }	
      result.push_back(' ');
    }

    //end the current row
    result.push_back('\n');

  }
  return result;
}
