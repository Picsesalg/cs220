#include <stdio.h>

//Alice Yang
//ayang36
//arithmetic.c
//Computes

int main() {

  printf("Please enter an arithmetic expression. Press Ctrl+f, Ctrl+d to exit.\n");

  char operand = 'a'; //The operators.
  int yes = 1; //Whether or not to continue.
  float total = 0;
  float next = 0; //The next number

  //Loop continues while there is still legal input.
  
  while (yes) {
    
    switch (operand) {
    case 'a': //determines first input
      if (scanf("%f", &next) != 1) { 
	printf("Malformed formula.\n");
	yes = 0;
	break;
      } else {
	total += next;
	operand = '~'; //Leads to default if unchanged
	scanf(" %c", &operand);
	break;
      }
    case '+':
      if (scanf("%f", &next) != 1) { 
	printf("Malformed formula.\n");
	yes = 0;
	break;
      } else {
	total += next;
	operand = '~';
	scanf(" %c", &operand);
	break;
      }
    case '-':
      if (scanf("%f", &next) != 1) {
	printf("Malformed formula.\n");
	yes = 0;
	break;
      } else {
	total -= next;
	operand = '~';
	scanf(" %c", &operand);
	break;
      }
    default:
      if (operand != '~') {
	printf("Malformed formula.\n");
	yes = 0;
	break;
      } else {
	printf("The answer is: %f.\n", total);
	yes = 0;
	break;
      }
    }
  }
  
  return 0;
}
