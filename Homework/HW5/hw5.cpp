#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::map;
using std::vector;

//For printing in alphabetical order
void print_a(map<string, int> bigram) {
  for (map<string, int>::const_iterator it = bigram.cbegin();
       it != bigram.cend();
       ++it) {
    cout << it->first << " " << it->second << endl;
  }
}

//For printing in reverse order
void print_r(map<string, int> bigram) {
  for (map<string, int>:: const_reverse_iterator it = bigram.crbegin();
       it != bigram.crend();
       ++it) {
    cout << it->first << " " << it->second << endl;
  }
}

//For printing by count frequency
void print_c(map<int, vector<string>> sort_by_count) {
  for (map<int, vector<string>>::const_reverse_iterator it = sort_by_count.crbegin();
       it != sort_by_count.crend();
       ++it) {
    for (vector<string>::const_iterator j = (it->second).cbegin();
	 j != (it->second).cend();
	 ++j) {
      cout << *j << " " << it->first << endl;
    }
  }
}

int main(int argc, char *argv[]) {

  //If there aren't the right number of arguments
  if (argc != 3) {
    cout << "Invalid arguments" << endl;
    return 1;
  }

  //If the print command isn't right
  if (*argv[2] != 'a' && *argv[2] != 'r' && *argv[2] != 'c') {
    cout << "Invalid arguments" << endl;
    return 1;
  }

  //First in the pair
  string first;
  //Second in the pair
  string second;
  //first and second concatenated
  string key;
  string temp;
  //The map to the bigram
  map<string, int> bigram;  

  cin >> first;
  cin >> second;

  //Converting the words all to lower case
  for (int i = 0; i < (int) first.length(); i++) {
    first[i] = tolower((int) first[i]);
  }
  for (int i = 0; i < (int) second.length(); i++) {
    second[i] = tolower((int) second[i]);
  }
  key = first + " " + second;

  //For finding.
  map<string, int>::iterator it;

  //Building the model
  bigram[key] = 1;

  //
  while (cin >> temp) {
    first = second;
    second = temp;
    for (int i = 0; i < (int) second.length(); i++) {
      second[i] = tolower((int) second[i]);
    }
    key = first + " " + second;
    //Finding a match for this pair
    it = bigram.find(key);
    //If it matches, then increment
    if (it != bigram.end()) {
      it->second++;
    }
    //If it doesn't match, then make a new value
    else {
      bigram[key] = 1;
    }
  }
  
  //For the last word, add "end"
  first = second;
  second = "end";
  key = first + " " + second;
  bigram[key] = 1;

  //Sort by count
  map<int, vector<string>> sort_by_count;

  for (map<string, int>::const_iterator i = bigram.cbegin();
       i != bigram.cend();
       ++i) {
    sort_by_count[i->second].push_back(i->first);
  }
  
  char print = *argv[2];

  //Printing types
  switch (print) {
  //Alphabetical
  case 'a':
    print_a(bigram);
    break;
  //Reverse
  case 'r':
    print_r(bigram);
    break;
  //By count
  case 'c':
    print_c(sort_by_count);
    break;
  }  

  //The seed word
  string seed = argv[1];
  //How many words are in the sentence
  int count = 20;
  //Whether or not there are words left
  char while_proceed = 'y';
  char for_proceed = 'y';

  //Checking whether or not a seed word is in the text.
  for (map<int, vector<string>>::const_reverse_iterator i = sort_by_count.crbegin();
       i != sort_by_count.crend();
       ++i) {
    for (vector<string>::const_iterator j = (i->second).cbegin();
	 j != (i->second).cend();
	 ++j) {
      if ((*j).find(seed) == 0) {
	count = 1;
	break;
      }
    }
    if (count == 1) {
      break;
    }  
  }

  //If seed word doesn't exist, then exit
  if (count != 1) {
    cout << "end" << endl;
    return 1;
  }
  
  while (count < 16 && seed != "end") {
    for_proceed = 'y';
    while_proceed = 'y';
    cout << seed << " ";
    for (map<int, vector<string>>::const_reverse_iterator i = sort_by_count.crbegin();
	 i != sort_by_count.crend();
	 ++i) {
      for (vector<string>::const_iterator j = (i->second).cbegin();
	   j != (i->second).cend();
	   ++j) {
	if ((*j).find(seed) == 0) {
	  std::size_t space = (*j).find(" ");
	  seed = (*j).substr(space + 1);
	  count++;
	  for_proceed = 'n';
	  while_proceed = 'n';
	  break;
	}
      }
      if (for_proceed != 'y') {
	break;
      }
    }
    if (seed == "end") {
      cout << "end " << endl;
    }
    if (while_proceed == 'y') {
      break;
    }
  }

  cout << endl;
  
  return 0;
}
