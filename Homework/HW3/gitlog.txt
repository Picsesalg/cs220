commit 30551a8e14bc9806e29aa63aa380ffef86b338e9
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Tue Feb 27 10:10:13 2018 -0500

    I'm done. I hope.

commit 35ea8c94ea8f3fbc373988e2dd4a66c6491cc18c
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Mon Feb 26 02:22:17 2018 -0500

    Cleaned up the files. Gotta do Readme and test.

commit c4909caa86ef79f534f29d3fe8a7843e39df05bb
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Mon Feb 26 01:42:03 2018 -0500

    Just need to fix up the errors, and do the tester file. X(

commit b0a1c2f5c20e949214380d367fb84123baf39eee
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 25 12:48:48 2018 -0500

    Got the array transfer good. Got the one word at a time good.

commit eb4f57875510d353e2d1bdde29c237a55a8c2433
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 25 10:35:49 2018 -0500

    Broke up user input into one word at a time.

commit 3bb4d908cec00eb5c6b495f44ad15475abc9e73b
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 25 09:54:01 2018 -0500

    Can get user input now.

commit 07b70da37ddf3bb7999490211ba1329f7054ad3a
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 25 09:18:11 2018 -0500

    Handled usual file errors.

commit db939eacd90e5b65e713e2d47311c49542dacb1e
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sat Feb 24 17:38:01 2018 -0500

    The necessary files

commit 78dfc7f16547c2c24437ef6087cec752e8c935fe
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Fri Feb 23 15:39:47 2018 -0500

    I've no idea how to do this.

commit e61f5c75cc07d24467677c6a0a5592a08d09f83e
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Fri Feb 23 08:41:24 2018 -0500

    Upload the OG files

commit 858f4ba50dd93db9c0bd1d05eb646c27371ede77
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sat Feb 17 14:01:00 2018 -0500

    3-2

commit cfaf07d9e503ff89bd0d94dbe508e47832ef088e
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 11 21:18:23 2018 -0500

    Everything's fixed and considered

commit e815035fb362fae6298a6cce15cb5b8b107b9510
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 11 20:36:33 2018 -0500

    Fixed changing operators.

commit 6e52c16ee36e9945d3fcf920ca1adc684d65748a
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 11 20:18:21 2018 -0500

    Kind of fixed up the looping twice. Maybe...

commit c4d8bb4a95b5c6bbf7aca57a75cd8cbcff54d3fa
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 11 16:07:59 2018 -0500

    Copied from the old file to this. Go to office hour about Ctrl-f and Ctrl-d.

commit f1701095dc88c2fa0c74f50f582ec45f1a1b7167
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 11 14:41:45 2018 -0500

    I fixed up the infinite loop problem.

commit 01642a2d956cf84a98428a76977cd1d9c192073a
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 11 14:38:21 2018 -0500

    I fixed up the infinite loop problem.

commit bcf76baab712919acbab2e5ce85f1e8854cf6cdd
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sun Feb 11 14:19:01 2018 -0500

    arithmetic.c

commit 0c7a8bb0ce6c0d268c93816b96360a779994e2d3
Author: Alice Yang <ayang36@ugradx.cs.jhu.edu>
Date:   Sat Feb 10 16:51:13 2018 -0500

    arithmetic.c

commit b8827486c9829f1f690aa7bd399750e2b6f8fa96
Author: Alice <ayang36@jhu.edu>
Date:   Tue Feb 6 10:14:25 2018 -0500

    20180206.c

commit 609043edad54d40bbaf37aa8543e892f036893c7
Author: Alice <ayang36@jhu.edu>
Date:   Tue Feb 6 10:01:32 2018 -0500

    20180206.c

commit 5dbd64ab2bee7fab3361c319ffd65e968f090ebc
Author: Alice <ayang36@jhu.edu>
Date:   Tue Feb 6 09:51:26 2018 -0500

    20180206.c

commit 30ec6609e236d3ab39d764451a29449d4cfc00c2
Author: Alice <ayang36@jhu.edu>
Date:   Tue Feb 6 09:36:23 2018 -0500

    20180206.c

commit 675ef933a49d21caa7161a915d3ee12aa209db27
Author: Alice <ayang36@jhu.edu>
Date:   Tue Feb 6 09:31:37 2018 -0500

    20180206.c

commit 207e70c103526d87c35b1a26681b50553e751609
Author: Alice <ayang36@jhu.edu>
Date:   Mon Feb 5 23:46:53 2018 -0500

    bronze.c

commit 0de77b6acbc8c3af0ab57e6f6cf1c247f3491dfa
Author: Alice <ayang36@jhu.edu>
Date:   Mon Feb 5 23:02:15 2018 -0500

    updated README

commit 2009b1b06b0943b666558366cee8d0d683fff520
Author: Alice <ayang36@jhu.edu>
Date:   Mon Feb 5 22:56:15 2018 -0500

    README
