/* dnasearch.h
   HW3
   Alice Yang
   ayang36
 */

#include <stdio.h>

/* To check the validity of the file.
   opened_file array for the file to be read
   Return 1 if invalid.
   Return 0 if valid.
*/
int alpha_valid(char *opened_file);

/* To copy the file into an array.
   to_read is the file to be read
   Return the array when done
 */
char *store_file(FILE *to_read);

/* To find matches for one pattern.
   opened_file the file we're searching through
   pattern the patterns we're trying to find a match for
   starting_index where in the file to start
   Return the index of the match.
 */
int matching(char *opened_file, char pattern[], int starting_index);
