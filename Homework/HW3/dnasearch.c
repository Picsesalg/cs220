/* dnasearch.c
   HW3
   Alice Yang
   ayang36
 */

#include "dnasearch.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/* To check the validity of the file.
   opened_file is the array for the file to be read from
   Return 1 if invalid
   Return 0 if valid
 */
int alpha_valid(char *opened_file) {

  int opened_file_length = (int) strlen(opened_file);
  
  for (int i = 0; i < opened_file_length; i++) {
    opened_file[i] = tolower(opened_file[i]);
    
    switch(opened_file[i]) {
    case 'a':
      break;
    case 'c':
      break;
    case 't':
      break;
    case 'g':
      break;
    case ' ':
      break;
    case '\n':
      break;
    default:
      return 1;
    }
  }
  return 0;
}

/* To copy the file into an array.
   to_read is the file to be read from
   Return the array after copied
*/
char *store_file(FILE *to_read) {

  char *opened_file = malloc(sizeof(char) * 15001);
  char buffer[100];
  
  while (fgets(buffer, 100, to_read)) {
    strcat(opened_file, buffer);
  }

  return opened_file;
}

/* To find matches for one pattern.
   opened_file the file we're searching through
   pattern the pattern we're finding a match for
   starting_index where we're starting from
   Return the index of the match.
*/
int matching(char *opened_file, char pattern[], int starting_index) {
  int pattern_length = (int) strlen(pattern);
  int opened_file_length = (int) strlen(opened_file);

  for (int i = starting_index; i < opened_file_length - pattern_length + 1; i++) {
    
    int j = 0;
    char true_or_false = 'T';
    do {
      if (opened_file[i + j] != pattern[j]) {
	j = pattern_length;
	true_or_false = 'F';
      }
      else {
	j++;
      }
    } while (j < pattern_length);

    if (true_or_false == 'T') {
      return i;
    }    
  }
  return -2;
}
