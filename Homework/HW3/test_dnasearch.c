/* dnasearch.h
   Alice Yang
   ayang36
   HW3
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "dnasearch.h"

int main() {
  /* To test the storing function
   */
  FILE *fp = fopen("README", "r");
  assert(store_file(fp) != 0);
  printf("The storing function works\n");

  /* To check whether the file array is valid.
   */
  char *readme = store_file(fp);
  assert(alpha_valid(readme) == 0);
  printf("Validating works\n");

  /* Checking the matching function works
   */
  char pattern[] = "ATAG";
  int i = 0;
  assert(matching(readme, pattern, i) != 1);
  printf("Matching works\n");


  printf("Success. You made it.\n");
  
  return 1;
}
