/* hw3.c
   HW3
   Alice Yang
   ayang36
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "dnasearch.h"

int main (int argc, char *argv[]) {

  // The file to be read from.
  FILE *to_read = NULL;

  /* Handling standard file related errors.
   */
  if (argc == 1) {
    fprintf(stderr, "[ERROR] No file name.\n");
    return 1;
  }
  else {
    to_read = fopen(argv[1], "r");
    if(!to_read) {
      fprintf(stderr, "[ERROR] Could not open file.\n");
      return 2;
    }
  }

  /* Stores the file into an array
   */
  char *opened_file = malloc(sizeof(char) * 15001);
  opened_file = store_file(to_read);
  int opened_file_length = (int) strlen(opened_file);
  
  /* Testing validity of file and converting 
     everything to lower case
   */
  if (alpha_valid(opened_file) || opened_file_length > 15000) {
    printf("Invalid text.\n");
    return 3;
  }
  
  /* Getting the patterns the user wants.
   */
  char *input = malloc(sizeof(char) * 15000);
  scanf("%[^\n]", input);
  int input_length = (int) strlen(input);

  /* Arranging the input into a smaller array
   */
  char patterns[input_length];
  for (int i = 0; i < input_length; i++) {
    patterns[i] = input[i];
  }
  free(input);

  /* Gets each pattern out one by one from 
     the user input
   */
  int i = 0;
  int when_to_stop = 0;
  char one_pattern[100];
  
  do {
    int j = 0;
    i = when_to_stop;
    
    while (patterns[i] != ' ' && i < input_length) {   
      one_pattern[j] = patterns[i];
      j++;
      i++;
    }
    one_pattern[j] = '\0';
    
    when_to_stop = (i + 1);

    printf("%s: ", one_pattern);

    /* Now checking through the program to see if things match or not.
       checking_index is where int he file to start reading from.
       true_or_false will reamin F unless a match is found. This will tell whether or not to print no match.
     */
    int checking_index = 0;
    char true_or_false = 'F';
    
    while ((checking_index + (int) strlen(one_pattern)) <= (opened_file_length - 1) && checking_index != -1) {
      int matched_index = matching(opened_file, one_pattern, checking_index);
      
      if (matched_index != -2) {
	printf("%d ", matched_index);
	true_or_false = 'T';
      }
      checking_index = matched_index + 1;
    }

    /* Finishing up, determining whether or not to go to new line and start 
       new pattern, or print no match then new pattern.
     */
    if (true_or_false == 'F') {
      printf("No match.\n");
    }
    else {
      printf("\n");
    }
  } while (when_to_stop < input_length + 1);

  free(opened_file);
  fclose(to_read);
  return 0;
}
