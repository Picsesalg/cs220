#include <stdio.h>

int main() {

  int sum = 0;

  while (1) {
    int addend = 0;
    if (scanf("%d", &addend) != 1) {
      break;
    }
    sum += addend;
  }
  printf("%d\n", sum);
  return 0;
}
