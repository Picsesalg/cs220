#include <stdio.h>
#include <stdlib.h>
#include "int_list.h"

void output_list(Node * head);
void output_list_recursively(Node * head);

int main() {

  //create first node in list
  Node* head = malloc(sizeof(Node));
  head->data = 5;

  //make first node point to new 2nd node
  head->next = malloc(sizeof(Node));

  //initialize values in 2nd node
  head->next->data = 12;
  head->next->next = NULL;  //2nd node will be last in list

  output_list(head);
  //  output_list_recursively(head);

  insert_after(head, 10);

  output_list(head);


  insert_after(head, 99);
  output_list(head);
  
}

void output_list(Node * head){
  Node * cur = head; //make cur point to same node that head points to
  while (cur != NULL) {
    printf("%d ", cur->data); //output int at the node cur points to
    cur = cur->next;  //advance cur to point to the next node in the list
  }
  printf("\n");
}


void output_list_recursively(Node * head){
  //base case
  if (head == NULL){  //list is empty
    printf("\n");     //just end the line
    return;
  }
  //recursive case: print one item, then make recursive call on smaller list
  printf("%d ", head->data);
  output_list_recursively(head->next);

}

