//header guard avoids duplicate definitions of structs
//if file is both directly #include-d and indirectly #include-d
//in the same file
#ifndef INT_LIST_H
#define INT_LIST_H


//Node container holds data and pointer to next Node in list
typedef struct _Node {
  int data;
  struct _Node * next;
} Node;

//Creates and inserts a new node containing value as its data,
//placing that new node just after the node pointed to by n.
//Returns 0 if successful, nonzero otherwise
int insert_after(Node * n, int value);


#endif
