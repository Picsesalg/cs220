#include <stdlib.h>
#include "int_list.h"
#include "assert.h"

//Creates and inserts a new node containing value as its data,
//placing that new node just after the node pointed to by n.
//Returns 0 if successful, nonzero otherwise
int insert_after(Node * n, int value) {
  Node * new_node = malloc(sizeof(Node));
  if (!new_node) {  //if new_node is NULL, then malloc failed
    return 1;
  }

  new_node->data = value; //put data into new node
  new_node->next = n->next; //make new one point to what n->next was pointing to
  n->next = new_node; //make n->next point to new item

  return 0;
}


//Creates and inserts a new node containing value as its data,
//placing that new node at the start of the list.
//Returns 0 if successful, nonzero otherwise
int insert_head(Node ** list_ptr, int value) {

  assert(list_ptr != NULL); //if it's NULL, then we don't have a head pointer

  //Create the new node and put data into it.
  Node * new_node = malloc(sizeof(Node));
  if (!new_node) {  //if new_node is NULL, then malloc failed
    return 1;
  }
  new_node-> data = value;

  //Make new node point to current first node (if any).
  //Note that we dereference list_ptr to get head pointer,
  //then make new_node->next point to what head points to.
  //This works even if head pointer is NULL.
  new_node->next = *list_ptr; 

  //Finally, change the head pointer to point to new node.
  //Note that we're changing the value that list pointer points to!
  *list_ptr = new_node;

  return 0; 
}

//Free the entire contents of the list, node by node
void free_list(Node * cur) {

  //Note that we have to save a pointer to next node before
  //we free the current one; otherwise we'll "orphan" nodes
  while(cur != NULL) {

    Node *next = cur->next; // *first* get `next`                                                      
    free(cur);              // *then* free `cur`                                                       
    cur = next;

  }
}  

