#include <stdio.h>
#include <stdbool.h>

int main() {

  int num_students = 32;
  unsigned not_negative = 2; //Integer starts from 0+
  //long long_number = 5823485; //Int that has more bits to store the number.
  float single_precise = 9354.1; //Less precise
  double double_precise = 2483.123; //More precise
  char character = 'a'; //A character. Works like unicde
  //bool can_or_not = true; //Sometimes 0=false, any other num = true

  int add = 3 + 4;
  int minus = 3 - 4;
  int times = 3 * 4;
  double divide = 3 / 4;
  double mod = 3 % 4;

  printf("There are %d number of students.\n", num_students);
  printf("num_students\n"); //This doesn't print out number
  printf("Unsigned is %d. Float is %0.1f. Double is %0.1f. Character is %c. Added is %d. Minus is %d. Times is %d. Divide is %0.2f. Mod is %0.2f.\n", not_negative, single_precise, double_precise, character, add, minus, times, divide, mod);

  int i;
  printf("Please enter an integer\n");
  scanf("%d", &i);
  printf("Your input was %d\n", i);

  int x;
  int y = 2;

  scanf("%d%d", &x, &y); //Reads input and stores them as x and y
  printf("x = %d, y = %d\n", x, y);
  
  return 0;
}
