#include <stdio.h>

int main() {

  FILE* input = fopen("numbers.txt", "r"); //open the file. r = read

  if (input == NULL) { //if there's no file called numbers.txt
    printf("Error: could ot read");
    return 1; //Indicates an error. Exits the program.
  }

  while (1) {
    int a = 0, b = 0;
    int ret = fscanf(input, "%d%d", &a, &b); //Scans the file for the first two integers. Stores as a &b
    if (ret == EOF) {
      break; //Exit loop.
    } else if (ret != 2){
      printf("Error: could not parse line\n");
      return 2; //indicates error
    }
    printf("%d\n", a + b);
  }

  if (ferror(input)) {
    printf("Error: error indicator was set for input file\n");
    return 3;
  }
  fclose(input); //CLOSE THE INPUT FILE AND SAVE TO THE FILE.
  return 0;
}
