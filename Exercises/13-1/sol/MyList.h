#ifndef MYLIST_H
#define MYLIST_H

#include <iostream>
#include "MyNode.h"

// A linked list template

template<typename T>
class MyList {

private:
    MyNode<T>* head; // pointer to first node in linked list

public:

    // *** DON'T CHANGE THIS FILE ABOVE THIS POINT ***

    // For the nested iterator class `iterator`, you will have to
    // provide implementations for the member functions commented out.
    //
    // For the nested iterator class `const_iterator`, you will have
    // to implement something like `iterator` but with additional
    // `const` constraints to disallow modification of the elements
    // via operator*().
    //
    // You will have to un-comment and implement the begin(),
    // end(), cbegin() and cend() member functions below
    //
    // You will also need to un-comment and implement the MyList
    // constructor taking a begin and end iterator.

    // *** solution code ***
    class iterator {
        MyNode<T>* it;

      public:
        iterator(MyNode<T>* initial) : it(initial) { }

        iterator& operator++() { it = it->next; return *this; }

        bool operator!=(const iterator& o) const { return it != o.it; }

        T& operator*() { return it->data; }
    };

    class const_iterator {
        MyNode<T>* it;

      public:
        const_iterator(MyNode<T>* pos) : it(pos) { }

        const_iterator& operator++() { it = it->next; return *this; }

        bool operator!=(const const_iterator& o) const { return it != o.it; }

        const T& operator*() const { return it->data; }
    };

    iterator begin() { return iterator(head); }
    iterator end() { return iterator(nullptr); }
    
    const_iterator cbegin() const { return const_iterator(head); }
    const_iterator cend() const { return const_iterator(nullptr); }

    // Constructor for MyList that sets the linked list
    // to contain the elements in the container with begin
    // and end iterators provided as arguments.
    template< typename Iterator >
    MyList< T >( Iterator iBegin , Iterator iEnd ) : head(nullptr)
    {
      for( Iterator it=iBegin ; it!=iEnd ; ++it ) insertAtTail( *it );
    }
    // *** end solution code ***

    // *** DON'T CHANGE THIS FILE BELOW THIS POINT ***

    MyList<T>() : head(nullptr) {}  // create empty linked list

    ~MyList<T>();                   // deallocate all nodes

    void insertAtHead(const T& d); // create new MyNode and add at head

    void insertAtTail(const T& d); // create new MyNode and add at tail

    // get const pointer to head node
    const MyNode<T>* get_head() const { return head; }
};

#include "MyList.inc"
#endif
