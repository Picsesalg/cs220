#include <iostream>
#include <string>
#include <map>
#include "card.h"

// `using` is OK here, since it's a .cpp file
using std::string;
using std::map;
using std::ostream;
using std::istream;

map<Suit, string> suit_to_string = {
    {Suit::HEART,   "Hearts"},
    {Suit::CLUB,    "Clubs"},
    {Suit::DIAMOND, "Diamonds"},
    {Suit::SPADE,   "Spades"} };

map<string, Suit> string_to_suit = {
    {"Hearts",   Suit::HEART},
    {"Clubs",    Suit::CLUB},
    {"Diamonds", Suit::DIAMOND},
    {"Spades",   Suit::SPADE} };

map<Rank, string> rank_to_string = {
    {Rank::ACE,   "Ace"},
    {Rank::TWO,   "Two"},
    {Rank::THREE, "Three"},
    {Rank::FOUR,  "Four"},
    {Rank::FIVE,  "Five"},
    {Rank::SIX,   "Six"},
    {Rank::SEVEN, "Seven"},
    {Rank::EIGHT, "Eight"},
    {Rank::NINE,  "Nine"},
    {Rank::TEN,   "Ten"},
    {Rank::JACK,  "Jack"},
    {Rank::QUEEN, "Queen"},
    {Rank::KING,  "King"} };

map<string, Rank> string_to_rank = {
    {"Ace",   Rank::ACE},
    {"Two",   Rank::TWO},
    {"Three", Rank::THREE},
    {"Four",  Rank::FOUR},
    {"Five",  Rank::FIVE},
    {"Six",   Rank::SIX},
    {"Seven", Rank::SEVEN},
    {"Eight", Rank::EIGHT},
    {"Nine",  Rank::NINE},
    {"Ten",   Rank::TEN},
    {"Jack",  Rank::JACK},
    {"Queen", Rank::QUEEN},
    {"King",  Rank::KING} };


// TODO: Add definition for operator<< (insertion) for Rank
//       Use map<Rank, string> rank_to_string, defined above
ostream& operator<<(ostream& os, Rank& r) {
  string rank = rank_to_string[r];
  os << rank;
  return os;
}

// TODO: Add definition for operator<< (insertion) for Suit
//       Use map<Suit, string> suit_to_string, defined above
ostream& operator<<(ostream& os, Suit& s) {
  string suit = suit_to_string[s];
  os << suit;
  return os;
}

// TODO: Add definition for operator<< (insertion) for Card
//       Use other insertion operators
ostream& operator<<(ostream& os, Card& c) {
  os << c.rank << " of " << c.suit;
  return os;
}

// TODO: add definitions for operator>> (extraction) for Rank
//       Use map<string, Suit> string_to_suit, defined above
istream& operator>>(istream& is, Rank& r) {
  string temp;
  is >> temp;
  r = string_to_rank[temp];  
  return is;
}

// TODO: add definitions for operator>> (extraction) for Rank
//       Use map<string, Suit> string_to_suit, defined above
istream& operator>>(istream& is, Suit& s) {
  string temp;
  is >> temp;
  is >> temp;
  is >> temp;
  s = string_to_suit[temp];
  return is;
}

// TODO: Add definition for operator>> (extraction) for Card
//       Use other extraction operators
istream& operator>>(istream& is, Card& c) {
  string temp;
  is >> c.rank;
  is >> temp;
  is >> c.suit;
  return is;
}
