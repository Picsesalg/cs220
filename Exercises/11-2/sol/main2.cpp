#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>

// *** solution code ***
#include <fstream>
// *** solution code ***

#include "card.h"

//
// Part 2: Implement straight_flush and file-based test cases
//
// TODO: Do all the TODOs in this file.
//       Note that this involves adding one more thing to
//       card.h/card.cpp (operator<).
//       Get this file to compile, run, and print "All assertions
//       passed."
//

using std::cout;
using std::endl;
using std::string;
using std::istream;
using std::stringstream;

// *** solution code ***
using std::ifstream;
// *** solution code ***

bool straight_flush(istream& is) {
    std::vector<Card> hand;
    
    // TODO: Read 5 cards from `is` into `hand`

    // *** solution code ***
    while(hand.size() < 5) {
        Card c; is >> c;
        hand.push_back(c);
    }
    // *** solution code ***
    
    // Use std::sort to sort the hand
    // TODO: This won't compile until you define operator< for Card
    std::sort(hand.begin(), hand.end());
    
    // TODO: Return true if hand is a straight flush, false otherwise
    //       A straight flush is a hand where (a) all the cards have
    //       consecutive rank (e.g. ace, two, three, four, five) and
    //       all are the same suit.  For purposes of this exercise,
    //       assume ace can be low (consecutive with two) but NOT high
    //       (not consecutive with king).  The fact that we've already
    //       sorted the hand should make this easier.
    
    // *** solution code ***
    bool straight = true, flush = true;
    {
        bool first = true;
        Suit last_suit = Suit::HEART;
        Rank last_rank = Rank::ACE;
        for(std::vector<Card>::const_iterator cit = hand.cbegin();
            cit != hand.cend(); cit++)
        {
            if(!first) {
                if(cit->suit != last_suit) {
                    flush = false;
                    break;
                }
                if((int)cit->rank != (int)last_rank+1) {
                    straight = false;
                    break;
                }
            }
            last_suit = cit->suit;
            last_rank = cit->rank;
            first = false;
        }
    }
    return straight && flush;
    // *** solution code ***
}

int main() {
    {
        // Test case 1
        stringstream ss("Eight of Hearts "  // when a string literal is
                        "Ten of Hearts "    // spread across lines like
                        "Jack of Hearts "   // this, the compiler
                        "Nine of Hearts "   // concatenates them into
                        "Queen of Hearts"); // 1 string
        assert(straight_flush(ss));
    }

    {
        // Test case 2
        stringstream ss("Eight of Hearts "
                        "Ten of Hearts "
                        "Jack of Clubs "
                        "Nine of Hearts "
                        "Queen of Hearts");
        assert(!straight_flush(ss));
    }

    {
        // Test case 3
        stringstream ss("Eight of Hearts "
                        "Ten of Hearts "
                        "King of Hearts "
                        "Nine of Hearts "
                        "Queen of Hearts");
        assert(!straight_flush(ss));
    }
    
    {
        // Test case 4
        // TODO: Write this using ifstream and case1.txt
        //       Don't use stringstream
        //       Reminder: #include <fstream> to use ifstream
        //       Result of calling straight_flush should be true
        
        // *** solution code ***
        ifstream ifs("case1.txt");
        assert(ifs && straight_flush(ifs));
        // *** solution code ***
    }

    {
        // Test case 5
        // TODO: Write this using ifstream and case2.txt
        //       Don't use stringstream
        //       Reminder: #include <fstream> to use ifstream
        //       Result of calling straight_flush should be false

        // *** solution code ***
        ifstream ifs("case2.txt");
        assert(ifs && !straight_flush(ifs));
        // *** solution code ***
    }

    {
        // Test case 6
        // TODO: Write this using ifstream and case3.txt
        //       Don't use stringstream
        //       Reminder: #include <fstream> to use ifstream
        //       Result of calling straight_flush should be false

        // *** solution code ***
        ifstream ifs("case3.txt");
        assert(ifs && !straight_flush(ifs));
        // *** solution code ***
    }

    cout << "All assertions passed" << endl;

    return 0;
}
