#include <iostream>
#include <string>
#include <map>
#include "card.h"

// `using` is OK here, since it's a .cpp file
using std::string;
using std::map;

using std::istream;
using std::ostream;

map<Suit, string> suit_to_string = {
    {Suit::HEART,   "Hearts"},
    {Suit::CLUB,    "Clubs"},
    {Suit::DIAMOND, "Diamonds"},
    {Suit::SPADE,   "Spades"} };

map<string, Suit> string_to_suit = {
    {"Hearts",   Suit::HEART},
    {"Clubs",    Suit::CLUB},
    {"Diamonds", Suit::DIAMOND},
    {"Spades",   Suit::SPADE} };

map<Rank, string> rank_to_string = {
    {Rank::ACE,   "Ace"},
    {Rank::TWO,   "Two"},
    {Rank::THREE, "Three"},
    {Rank::FOUR,  "Four"},
    {Rank::FIVE,  "Five"},
    {Rank::SIX,   "Six"},
    {Rank::SEVEN, "Seven"},
    {Rank::EIGHT, "Eight"},
    {Rank::NINE,  "Nine"},
    {Rank::TEN,   "Ten"},
    {Rank::JACK,  "Jack"},
    {Rank::QUEEN, "Queen"},
    {Rank::KING,  "King"} };

map<string, Rank> string_to_rank = {
    {"Ace",   Rank::ACE},
    {"Two",   Rank::TWO},
    {"Three", Rank::THREE},
    {"Four",  Rank::FOUR},
    {"Five",  Rank::FIVE},
    {"Six",   Rank::SIX},
    {"Seven", Rank::SEVEN},
    {"Eight", Rank::EIGHT},
    {"Nine",  Rank::NINE},
    {"Ten",   Rank::TEN},
    {"Jack",  Rank::JACK},
    {"Queen", Rank::QUEEN},
    {"King",  Rank::KING} };


// TODO: Add definition for operator<< (insertion) for Rank
//       Use map<Rank, string> rank_to_string, defined above

ostream& operator<<(ostream& os, Rank r) {
    os << rank_to_string[r];
    return os;
}

// TODO: Add definition for operator<< (insertion) for Suit
//       Use map<Suit, string> suit_to_string, defined above

ostream& operator<<(ostream& os, Suit s) {
    os << suit_to_string[s];
    return os;
}

// TODO: Add definition for operator<< (insertion) for Card
//       Use other insertion operators

ostream& operator<<(ostream& os, Card c) {
    os << c.rank << " of " << c.suit;
    return os;
}


// TODO: add definitions for operator>> (extraction) for Rank
//       Use map<string, Suit> string_to_rank, defined above

istream& operator>>(istream& is, Rank& r) {
    string str;
    is >> str;
    r = string_to_rank[str];
    return is;
}

// TODO: add definitions for operator>> (extraction) for Suit
//       Use map<string, Suit> string_to_suit, defined above

istream& operator>>(istream& is, Suit& s) {
    string str;
    is >> str;
    s = string_to_suit[str];
    return is;
}

// TODO: Add definition for operator>> (extraction) for Card
//       Use other extraction operators

istream& operator>>(istream& is, Card& c) {
    is >> c.rank;
    string tmp; is >> tmp;
    is >> c.suit;
    return is;
}
