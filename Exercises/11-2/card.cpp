#include <iostream>
#include <string>
#include <map>
#include "card.h"

// `using` is OK here, since it's a .cpp file
using std::string;
using std::map;

map<Suit, string> suit_to_string = {
    {Suit::HEART,   "Hearts"},
    {Suit::CLUB,    "Clubs"},
    {Suit::DIAMOND, "Diamonds"},
    {Suit::SPADE,   "Spades"} };

map<string, Suit> string_to_suit = {
    {"Hearts",   Suit::HEART},
    {"Clubs",    Suit::CLUB},
    {"Diamonds", Suit::DIAMOND},
    {"Spades",   Suit::SPADE} };

map<Rank, string> rank_to_string = {
    {Rank::ACE,   "Ace"},
    {Rank::TWO,   "Two"},
    {Rank::THREE, "Three"},
    {Rank::FOUR,  "Four"},
    {Rank::FIVE,  "Five"},
    {Rank::SIX,   "Six"},
    {Rank::SEVEN, "Seven"},
    {Rank::EIGHT, "Eight"},
    {Rank::NINE,  "Nine"},
    {Rank::TEN,   "Ten"},
    {Rank::JACK,  "Jack"},
    {Rank::QUEEN, "Queen"},
    {Rank::KING,  "King"} };

map<string, Rank> string_to_rank = {
    {"Ace",   Rank::ACE},
    {"Two",   Rank::TWO},
    {"Three", Rank::THREE},
    {"Four",  Rank::FOUR},
    {"Five",  Rank::FIVE},
    {"Six",   Rank::SIX},
    {"Seven", Rank::SEVEN},
    {"Eight", Rank::EIGHT},
    {"Nine",  Rank::NINE},
    {"Ten",   Rank::TEN},
    {"Jack",  Rank::JACK},
    {"Queen", Rank::QUEEN},
    {"King",  Rank::KING} };


// TODO: Add definition for operator<< (insertion) for Rank
//       Use map<Rank, string> rank_to_string, defined above

// TODO: Add definition for operator<< (insertion) for Suit
//       Use map<Suit, string> suit_to_string, defined above

// TODO: Add definition for operator<< (insertion) for Card
//       Use other insertion operators


// TODO: add definitions for operator>> (extraction) for Rank
//       Use map<string, Suit> string_to_rank, defined above

// TODO: add definitions for operator>> (extraction) for Suit
//       Use map<string, Suit> string_to_suit, defined above

// TODO: Add definition for operator>> (extraction) for Card
//       Use other extraction operators
