#include <iostream>
#include "account_simple.h"

//
// Chapter 3: A tale of 2 sizeofs, part one
//

// We learned about sizeof in the first half the class, and used
// it to measure the sizes of various types including structs.
// Recall that sizeof is a "built-in" and not really a function;
// that's why it's OK to call sizeof(node) or sizeof(struct Node)
// (i.e. you can pass it a variable *or a type*, unlike a
// function). 

// Resolve the TODOs in the comments below and then think about
// and answer these questions:
// - Why are the sizeof results what they are?  What is sizeof
//   adding up?  Are member functions of a class included in
//   the total size of the class?  Are the fields in the parent
//   class included in the total for the derived class?
// - Would the result of the sizeof change if we passed these
//   variables by value rather than by reference?

using std::cout;
using std::endl;

void print_sizes_1(const Account& acct,
                   const CheckingAccount& checking,
                   const SavingsAccount& saving)
{
    // TODO use cout & sizeof to print the sizes of these objects
}

void print_sizes_2(const Account& acct,
                   const Account& checking,
                   const Account& saving)
{
    // TODO use cout & sizeof to print the sizes of these objects
}

int main() {
  Account acct(1000.0);
  CheckingAccount checking(1000.0, 2.00);
  SavingsAccount saving(1000.0, 0.05);

  // TODO: call print_sizes_1 and print_sizes_2 with appropriate
  //       arguments to print the object sizes
  
  return 0;
}
