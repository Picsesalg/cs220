// [TODO 2]
// Put your stack member function definitions here

#include <cassert>
#include "stack.h"

template <typename T>
void stack<T>::push(T i) {
     assert(_reserved > 0);
     if (_reserved == _size) {
     	_reserved *= 2;
	T *_temp_data = new T[_reserved];
	for (int j = 0; j < _size; j++) {
	    _temp_data[j] = _data[j];
	}
	delete[] _data;
	_data = _temp_data;
     }
     _data[_size++] = i;
}

template <typename T>
T stack<T>::pop() {
     assert(_size > 0);
     _size--;
     return _data[_size];
}
