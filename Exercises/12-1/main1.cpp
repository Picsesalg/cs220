#include <iostream>
#include <string>
#include "int_stack.h"

int main( void )
{
  int_stack iStack;
  
  // Add five ints onto the stack
  for( int i=0 ; i<5 ; i++ ) iStack.push(i);

  // Remove the elements from the stack and print out their values
  for( int i=0 ; i<5 ; i++ )
  {
    int j = iStack.pop();
    std::cout << "[" << i << "] " << j << std::endl;
  }
  return 0;
}

