#include <iostream>
#include <string>
#include "stack.h"
#include "card.h"

template< typename D >
void pop_all_and_print( stack< D >& dStack )
{
  while( !dStack.empty() )
  {
    D d = dStack.pop();
    std::cout << "\t" << d << std::endl;
  }
}
int main( void )
{
  stack< int > iStack;
  stack< Card > cStack;
  for( int i=0 ; i<5 ; i++ )
  {
    iStack.push(i);
    cStack.push( Card( Rank((int)Rank::ACE+i) , Suit::HEART ) );
  }

  std::cout << "Printing integers:" << std::endl;
  pop_all_and_print( iStack );

  std::cout << "Printing cards:" << std::endl;
  pop_all_and_print( cStack );
  return 0;
}

