#ifndef INT_STACK_H
#define INT_STACK_H

// An integer stack class

class int_stack
{
private:
  int _size;     // The number of elements in the stack
  int _reserved; // The amount of elements the stack should be able to store
                 // This should always be greater than zero and at least as big as _size
  int* _data;    // The storage for elements in the stack
public:
  // The default constructor creates an empty stack with enough storage to store one element
  int_stack( void ) : _size(0) , _reserved(1) , _data(new int[_reserved]) {}

  // This deallocates all the memory associated with the stack
  ~int_stack( void ) { delete[] _data; }

  // Indicates whether or not there are any elements on the stack
  bool empty( void ) const { return _size==0; }

  // Pops off the most recently entered element of the stack
  // The assumption is that there is at least one element on the stack
  int pop( void );

  // Pushes an element onto the back of the stack
  void push( int i );
};

#endif // INT_STACK_H
