#include <cassert>
#include "int_stack.h"

int int_stack::pop( void )
{
  assert( _size>0 );
  _size--;
  return _data[_size];
}
void int_stack::push( int i )
{
  assert( _reserved>0 );
  if( _reserved==_size )
  {
    _reserved *= 2;
    int* _temp_data = new int[ _reserved ];
    for( int i=0 ; i<_size ; i++ ) _temp_data[i] = _data[i];
    delete[] _data;
    _data = _temp_data;
  }
  _data[_size++] = i;
}
