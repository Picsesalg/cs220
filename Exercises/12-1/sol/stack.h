#ifndef STACK_H
#define STACK_H

// A generic stack class

template< typename Data >
class stack
{
private:
  int _size;     // The number of elements in the stack
  int _reserved; // The amount of elements the stack should be able to store
                 // This should always be greater than zero and at least as big as _size
  Data* _data;   // The storage for elements in the stack
public:
  // The default constructor creates an empty stack with enough storage to store one element
  stack( void ) : _size(0) , _reserved(1) , _data(new Data[_reserved]) {}

  // This deallocates all the memory associated with the stack
  ~stack( void ) { delete[] _data; }

  // Indicates whether or not there are any elements on the stack
  bool empty( void ) const { return _size==0; }

  // Pops off the most recently entered element of the stack
  // The assumption is that there is at least one element on the stack
  Data pop( void );

  // Pushes an element onto the back of the stack
  void push( Data d );
};
#include "stack.inc"
#endif // STACK_H
