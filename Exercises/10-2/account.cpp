#include "account.h"

using std::ostream;
using std::cout;
using std::endl;

ostream& operator<<(ostream& os, Account& acct) {
  os << acct.type() << ' ';
  return os;
}

ostream& operator<<(ostream& os, CheckingAccount& cacct) {
  os << cacct.type() << ' ';
  return os;
}

ostream& operator<<(ostream& os, SavingsAccount& sacct) {
  os << sacct.type() << ' ';
  return os;
}
