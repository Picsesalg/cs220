#include <stdio.h>
#include <string.h>
#include <ctype.h>

// stdio.h gives us printf
// string.h gives us strlen

int main() {
    const char text[] = "4 score and 7 years ago our fathers brought "
                       "4th on this continent, a new nation, "
                       "conceived in Liberty, and dedic8d to the "
                       "proposition that all men are cre8d =";

    int ascii_count[256] = {0};
    int text_len = strlen(text);

    // TODO A: with a single loop, count the # occurrences of
    //         each ascii character

    // HINT: use each char of the text as an offset into the
    //       ascii_count array, then update using increment (++)

    // 
    //
    //
    for (int i = 0; i < text_len; i++) {
      char jetzt = text[i];
      int nummer = jetzt;
      ascii_count[nummer]++;
    }

    char top_char = '\0';
    int top_freq = 0;

    char next_char = '\0';
    int next_freq = 0;

    // TODO B: With a single loop find the most frequent and
    //         second-most-frequent characters in the text.
    //         Store most frequent character and its frequency
    //         in top_char and top_freq.
    //         Store second-most-frequent character and its
    //         frequency in next_char and next_freq.

    //
    //
    //

    for (int i = 0; i < 256; i++) {
      if (ascii_count[i] > top_freq) {
	top_char = (char) i;
	top_freq = ascii_count[i];
      } else if (ascii_count[i] > next_freq) {
	next_char = (char) i;
	next_freq = ascii_count[i];
      }
    }

    printf("'%c' occurred %d times, '%c' occurred %d times\n",
	   top_char, top_freq, next_char, next_freq);
    return 0;
}
