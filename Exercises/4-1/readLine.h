#ifndef READLINE_H
#define READLINE_H
#include <stdio.h>
/*
 * This function reads the next line from the file handle fp.
 * (That is, it reads either up to the next newline character, or to the end of the file, whichever comes first.)
 * If fp is at the end of the file when the function is called, the function returns NULL.
 * Otherwise, it returns the null-terminated string that was read (including the newline character)
 */
char* readLine( FILE* fp );
#endif // READLINE_H
