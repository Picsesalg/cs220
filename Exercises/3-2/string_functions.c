#include <stdio.h>
#include <string.h>
#include "string_functions.h"

/*
    Returns in the third argument the concatenation of the first
    argument and the second argument, provided that there is
    sufficient space in third argument, as specified by the fourth.
    e.g.
        concat("alpha", "beta", result, 10) puts "alphabeta" into result and returns 0
        concat("alpha", "gamma", result, 10) puts nothing into result and returns 1
*/
int concat(const char word1[], const char word2[], char result[], int result_capacity){

  int total_chars = strlen(word1) + strlen(word2);
  int word1_chars = strlen(word1);
   //TODO: replace the following stub with a correct function body so that 
   //      this function behaves as indicated in the comment above
   //
   //NOTE: you may not use the strcat or strcpy library functions in your solution!
  if (total_chars  >= result_capacity) {
    return 1;
  } else if (total_chars  < result_capacity) {
    for (int i = 0; i < total_chars; i++) {
      if (i < word1_chars) {
	result[i] = word1[i];
      } else {
	result[i] = word2[i - word1_chars];
	printf("%c", result[i]);
      }
    }
    result[total_chars] = '\0';
  }
  return 0;
}
