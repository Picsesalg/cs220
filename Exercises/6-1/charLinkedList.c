#include <stdio.h>
#include <stdlib.h>
#include "charLinkedList.h"

Node* create( char value )
{
  Node * node = (Node*)malloc( sizeof( Node ) );
  if( node )
  {
    node->next = NULL;
    node->value = value;
  }
  return node;
}

int insert_after( Node * node , char value )
{
  Node * newNode = create( value );
  if( !newNode ) return 0;
  newNode->next = node->next;
  node->next = newNode;
  return 1;
}

int insert_at_head( Node ** list_ptr , char value )
{
  Node * newHead = create( value );
  if( !newHead ) return 0;
  newHead->next = *list_ptr;
  *list_ptr = newHead;
  return 1;
}

int insert_in_order( Node ** list_ptr , char value ) {
  
  Node *temp = malloc(sizeof(Node));
  temp = create(value);

  Node *cur = malloc(sizeof(Node));
  cur = *list_ptr;
  
  //If malloc fails.
  if (!temp) {
    free(temp);
    free(cur);
    return 0;
  }
  
  //Constructing the first node.
  if (*list_ptr == NULL) {
    *list_ptr = temp;
    free(cur);
    free(temp);
    return 1;
  }

  //Whether or not the while loop proceeds
  int can = 1;

  while (can) {
    //If there's only one node, or we've reached the last node
    if (cur->next == NULL) {
      if (temp->value < cur->value) {
	temp->next = *list_ptr;
	*list_ptr = temp;
	return 1;
      }
      else {
	cur->next = temp;
	return 1;
      }
    }
    //If the value is the first in the whole list
    else if (temp->value <= (*list_ptr)->value) {
      temp->next = *list_ptr;
      *list_ptr = temp;
      return 1;
    }
    else if (value >= cur->value && value < (cur->next)->value){
      temp->next = cur->next;
      cur->next = temp;
      return 1;
    }
    else {
      cur = cur->next;
    }
  }
  return 0;
}

void remove_after( Node * node ) {
  // [WARNING] This is not implemented.
  Node *cur = malloc(sizeof(Node));
  //Holds the node to be removed.
  cur = node->next;
  //Points to the node after the one removed
  node->next = cur->next;

  free(node->next);
  free(cur);
}

void remove_at_head( Node ** list_ptr ) {

  Node *cur = malloc(sizeof(Node));

  cur = (*list_ptr)->next;

  *list_ptr = cur->next;

  free(cur);
}

void remove_all( Node ** list_ptr , char value )
{
  // [WARNING] This is not implemented.
  Node *cur = malloc(sizeof(Node));
  Node *temp = malloc(sizeof(Node));
  
  cur = *list_ptr;
  
  while (cur != NULL && cur->next != NULL) {
    if ((cur->next)->value == value) {
      temp = cur->next;
      cur = temp->next;
      printf("value %c\n", value);
      if (cur->next != NULL) {
	cur = cur->next;
      }
    }
    else {
      cur = cur->next;
    }
  }
  //if (cur->next == NULL && cur->value == value) {
  //cur = cur->next;
  //}
  free(temp);
}

void print_list( const Node * head )
{
  for( const Node * n=head ; n ; n=n->next ) printf( "%c" , n->value );
}

void free_list( Node * head )
{
  while( head )
  {
    Node * newHead = head->next;
    free( head );
    head = newHead;
  }
}
