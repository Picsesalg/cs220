#include <stdio.h>
#include <stdlib.h>
#include "charLinkedList.h"

Node* create( char value )
{
  Node * node = (Node*)malloc( sizeof( Node ) );
  if( node )
  {
    node->next = NULL;
    node->value = value;
  }
  return node;
}

int insert_after( Node * node , char value )
{
  Node * newNode = create( value );
  if( !newNode ) return 0;
  newNode->next = node->next;
  node->next = newNode;
  return 1;
}

int insert_at_head( Node ** list_ptr , char value )
{
  Node * newHead = create( value );
  if( !newHead ) return 0;
  //Assigns address of newHead->next to the address held in head.
  //Dereferenced list_ptr, which is the stuff in head, which is the address of the first node. 
  newHead->next = *list_ptr;
  //The dereferenced *list_ptr is the address held in head.
  //So the address head is holding is the newHead. 
  *list_ptr = newHead;
  return 1;
}

int insert_in_order( Node ** list_ptr , char value )
{

  //When head is null. There's nothing in the list
  if (!(*list_ptr)) {
    *list_ptr = create (value);
    if (!(*list_ptr)) {
      return 0;
    }
    else {
      return 1;
    }
  }

  //Inserting at the start of the list.
  //(*list_ptr)->value means:
    //(*list_ptr) talking about the pointer head with value NULL, and next to the next node.
    //But then -> says point to the value of the node that head is pointing to.
    //If written (*list_ptr).value, then refers to the value held at help, which is NULL.
  else if (value < (*list_ptr)->value) {
    return insert_at_head(list_ptr, value);
  }

  //Inserting elsewhere in the list.
  else {
    //So cur points to head, which has the address of the first node,
    //So cur points to the first node.
    Node *cur = *list_ptr;
    //No seg fault because we first checked cur->next !=NULL
    //Moves left to right so no seg fault.
    //Why (cur->next)->value and not (cur->next).value because cur->next is a part of the struct and it's just a pointer. So it's referencing to the value held at the address pointed to by next
    while (cur->next != NULL && (cur->next)->value < value) {
      cur = cur->next;
    }
  }
  return insert_at_head( list_ptr , value );
}

void remove_after( Node * node )
{
  // [WARNING] This is not implemented.
}

void remove_at_head( Node ** list_ptr )
{
  // [WARNING] This is not implemented.
}

void remove_all( Node ** list_ptr , char value )
{
  // [WARNING] This is not implemented.
}

void print_list( const Node * head )
{
  for( const Node * n=head ; n ; n=n->next ) printf( "%c" , n->value );
}

void free_list( Node * head )
{
  while( head )
  {
    Node * newHead = head->next;
    free( head );
    head = newHead;
  }
}
